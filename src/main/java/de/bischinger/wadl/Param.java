package de.bischinger.wadl;

import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 
 * @author Alex Bischof 
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.PARAMETER })
public @interface Param
{
	String value();

	String style() default "query";

	boolean required() default false;

	Class<? extends Annotation>[] optionLiteral() default {};
}
