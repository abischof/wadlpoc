package de.bischinger.wadl;

import de.bischinger.wadl.model.Application;
import de.bischinger.wadl.model.Resources;

import java.util.List;

import javax.enterprise.inject.Any;
import javax.enterprise.inject.Instance;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;

/**
 * 
 * @author Alex Bischof 
 */
public class WadlApplicationProducer
{
	@Inject
	@Any
	@Wadl
	Instance<List<Resources>> resourcesInstances;

	@Inject
	@Wadl
	@BaseUri
	String baseUri;

	@Produces
	@Wadl
	@WadlApplication
	public Application create()
	{
		Application ret = new Application();
		ret.getResources().addAll(resourcesInstances.get());

		return ret;
	}
}
