package de.bischinger.wadl;

import java.lang.annotation.Annotation;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.enterprise.inject.Any;
import javax.enterprise.inject.Instance;
import javax.enterprise.inject.Produces;
import javax.enterprise.util.AnnotationLiteral;
import javax.inject.Inject;
import javax.ws.rs.HttpMethod;
import javax.ws.rs.Path;

import de.bischinger.wadl.model.*;
import org.apache.commons.collections.CollectionUtils;



/**
 * 
 * @author Alex Bischof 
 */
public class WadlResourcesProducer
{
	@Inject
	@Wadl
	@Any
	Instance<Class<?>[]> classInstances;

	@Inject
	@Wadl
	@Options
	Instance<Collection<String>> optionHolderInstances;

	@Produces
	@Wadl
	public List<Resources> create()
	{
		List<Resources> resouresList = new ArrayList<Resources>();

		for (Class<?>[] resources : classInstances)
		{
			//Create resource per class
			for (Class<?> clazz : resources)
			{
				Resources wadlResources = new Resources();
				resouresList.add(wadlResources);

				//reads path value
				Path pathAnnotation = clazz.getAnnotation(Path.class);
				assert pathAnnotation != null : "Demo-Assumption need path at class";
				String pathString = pathAnnotation.value();
				wadlResources.setBase(pathString);

				/*
				 * Creates WADL-Methods from clazz-methods.
				 * 
				 * Note: implementation uses currently all public, non static methods. 
				 */
				java.lang.reflect.Method[] declaredMethods = clazz.getDeclaredMethods();
				for (java.lang.reflect.Method method : declaredMethods)
				{
					//Filter for public non static methods
					int modifiers = method.getModifiers();
					if(!Modifier.isPublic(modifiers) || Modifier.isStatic(modifiers))
					{
						continue;
					}

					//Creates Resource
					Resource wadlResource = new Resource();
					wadlResources.getResource().add(wadlResource);

					pathAnnotation = method.getAnnotation(Path.class);
					pathString = pathAnnotation == null ? "/" : pathAnnotation.value();
					wadlResource.setPath(pathString);

					//Creates WADL-Method
					Method wadlMethod = new Method();
					wadlResource.getMethodOrResource().add(wadlMethod);

					String httpMethodString = getHttpMethodString(method);
					wadlMethod.setName(httpMethodString);

					//Note: currently vulnerable to overloading
					String methodId = method.getDeclaringClass().getSimpleName() + "." + method.getName();
					wadlMethod.setId(methodId.toLowerCase());

					/*
					 * Reads method parameter and creates request/parameters.
					 */
					createWadlParameters(method, wadlMethod);
				}
			}
		}

		return resouresList;
	}

	private void createWadlParameters(java.lang.reflect.Method method, Method wadlMethod)
	{
		Annotation[][] parameterAnnotations = method.getParameterAnnotations();
		for (Annotation[] annotations : parameterAnnotations)
		{
			for (Annotation annotation : annotations)
			{
				//Reads parameter details if exists
				if(annotation.annotationType().equals(Param.class))
				{
					Param paramAnno = (Param) annotation;

					//Creates request
					Request wadlRequest = new Request();
					wadlMethod.setRequest(wadlRequest);

					//creates wadl-parameter
					de.bischinger.wadl.model.Param wadlParam = new de.bischinger.wadl.model.Param();
					wadlRequest.getParam().add(wadlParam);
					wadlParam.setName(paramAnno.value());
					wadlParam.setRequired(paramAnno.required());

					//Note currently only query parameter are supported
					wadlParam.setStyle(ParamStyle.QUERY);

					//Checks for Options
					Class<? extends Annotation>[] optionLiterals = paramAnno.optionLiteral();
					if(optionLiterals != null && optionLiterals.length > 0)
					{
						Instance<Collection<String>> tmpInstance = optionHolderInstances;
						for (final Class<? extends Annotation> optionLiteral : optionLiterals)
						{
							AnnotationLiteral<Annotation> annotationLiteral = new AnnotationLiteral<Annotation>()
							{
								@Override
								public Class<? extends Annotation> annotationType()
								{
									return optionLiteral;
								}
							};
							tmpInstance.select(annotationLiteral);
						}
						Collection<String> optionCollection = tmpInstance.get();
						if(!CollectionUtils.isEmpty(optionCollection))
						{
							for (String string : optionCollection)
							{
								Option option = new Option();
								option.setValue(string);
								wadlParam.getOption().add(option);
							}
						}
					}
				}
			}
		}
	}

	/**
	 * Reads http method from method. If null reads it from class.
	 * 
	 * @param method
	 * @return
	 */
	private String getHttpMethodString(java.lang.reflect.Method method)
	{
		//JAXRS default
		String httpMethodString = HttpMethod.GET;

		//Gets HTTPMethod from Method
		Annotation[] annotations = method.getAnnotations();
		HttpMethod annotatedHttpMethod = getHttpMethod(annotations);

		if(annotatedHttpMethod == null)
		{
			//Get HttpMethod from Class
			annotatedHttpMethod = getHttpMethod(method.getDeclaringClass().getAnnotations());
		}

		if(annotatedHttpMethod != null)
		{
			httpMethodString = annotatedHttpMethod.value();
		}
		return httpMethodString;
	}

	/**
	 * Gets HttpMethod-Annotation from annotation array.
	 * 
	 * @param annotations
	 * @return
	 */
	private HttpMethod getHttpMethod(Annotation[] annotations)
	{
		//TODO jdk8
		HttpMethod annotatedHttpMethod = null;
		for (Annotation annotation : annotations)
		{
			HttpMethod httpMethod = annotation.annotationType().getAnnotation(HttpMethod.class);
			if(httpMethod != null)
			{
				annotatedHttpMethod = httpMethod;
				break;
			}
		}
		return annotatedHttpMethod;
	}
}
